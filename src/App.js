import logo from "./logo.svg";
import "./App.css";
import HeaderComponent from "./component/HeaderComponent/HeaderComponent.js";
import BodyComponent from "./component/BodyComponent/BodyComponent.js";
import FooterComponent from "./component/FooterComponent/FooterComponent";

function App() {
  return (
    <div className="App">
      <HeaderComponent />
      <BodyComponent />
      <FooterComponent />
    </div>
  );
}

export default App;
